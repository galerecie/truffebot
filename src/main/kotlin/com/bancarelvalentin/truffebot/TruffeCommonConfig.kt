package com.bancarelvalentin.truffebot

import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.default.features.dj.DjEmoteAction
import com.bancarelvalentin.ezbot.utils.DjUtils
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.IDENTITY

/**
 * Config for truffe bot required for the use in parallel with avast bot
 */
@Suppress("unused")
class TruffeCommonConfig : CommonConfig(IDENTITY.TRUFFE) {
    
    override val supportLazyCommands = true
    override val disableLiveConfigEdit = false
    override val djDedicatedChannelId = if (EnvConfig.DEV) 955180385380225054 else 958101925772029983
    override val supportDjDedicatedChannel = true
    override val djDedicatedChannelExtraActionsEmotes = arrayOf(
        DjEmoteAction(EmojiEnum.ANCHOR, "Charger la playlist GLRC") { user, _, event ->
            DjUtils.queueShortcut(event, user, "https://www.youtube.com/playlist?list=PLtBDu2VGLSlqN55eaWZ3cM8RKUH3TOFEM")
        })
    
    override val prefixes = arrayOf("t.", "/", *commonPrefixes)
    override val defaultColorHex = "#F2EDED"
}
